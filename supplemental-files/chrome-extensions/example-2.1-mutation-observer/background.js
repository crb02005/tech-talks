(function(){
    // const getCurrentTab =async ()=> {
    //     let queryOptions = { active: true, currentWindow: true };
    //     let [tab] = await chrome.tabs.query(queryOptions);
    //     return tab;
    //   }
    const sendToContent = async (tabId, messageType, data=null)=>{
        try {
            return await chrome.tabs.sendMessage(tabId,[
                messageType,
                data
            ]);
        } catch(er){
            // Yum tasty errors, this is a program for demo, in a real program most likely you would not eat errors.
            return null;
        }
    }
    const requestHandlers = {"imageCount":(sender,data)=>{
        console.log(`new image count ${data.imageCount}`);
        chrome.browserAction.setBadgeText({text:data.imageCount.toString()});
    }};

    console.log('adding a badgeClick handler');
    chrome.browserAction.onClicked.addListener((sender)=>{
        sendToContent(sender.id,"badgeClicked",{});
    })

    console.log('adding a tabBackground handler');
    chrome.tabs.onActivated.addListener((activeInfo)=>{
        console.log('activated a tab background');
        chrome.browserAction.setBadgeText({text:""});
        sendToContent(activeInfo.tabId,"activatedTab",{});
    })

    console.log('adding a message received from content script handler');
    chrome.runtime.onMessage.addListener((message,sender)=>{
        // didn't add a console log because it would be spammy
        // we have one listener and it routes to the ones in the dictionary above
        const [messageType,data]=message;
        requestHandlers[messageType](sender,data);
    });
}());
