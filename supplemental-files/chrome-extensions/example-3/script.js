(function(){
    let items = [];
    let count = 0;

    const updateItems = ()=>{
        items=document.getElementsByTagName("img");
    }
    const requests = {
        "badgeClicked":(data)=>{
            alert("You clicked on the badge");
        },
        "images":(data)=>{
            console.log("images was called");
            let result = "";
            for(let i = 0; i < items.length; i+=1){
                result +=items[i].src;
                result +="\r\n"
            }
            console.log("images result",result);
            console.log(result);
            return result;
        },
        "activatedTab":(data)=>{
            // Switching from the lazy, naive approach makes us need to update when the tab changes, so we are subscribing in the background and passing the message here.
            console.log("tab activated - notified by background tab, and handling it in the content script.");
            updateItems();
            count = items.length;
            sendToBackground("imageCount",{imageCount:count});   
        }};

    const sendToBackground = async (messageType, data=null)=>{
       /* This sends a message from the content script to the background script */
        try {
            return await chrome.runtime.sendMessage([
                messageType,
                data
            ],(response)=>{
                //in this current pattern we aren't doing anything with the response, we could enhance the pattern to make use of it
                // right now this response is null
                //console.log(response);
            });
        } catch(er){
            console.error("failed to sendToBackground, error:",er);
            return null;
        }
    };

    //Wire up our messages from chrome to our listeners.
    // There are rules on which script can do what so we pass messages.
    chrome.runtime.onMessage.addListener((message,sender,respond) => {
        const [ messageType, data ] = message;
        let r =requests[messageType](data);
        if(respond){
            console.log('respond was passed so calling it');
            respond(r);
        }
    });


    //https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver
    const targetNode = document;
    const config = { attributes: false, childList: true, subtree: true };

    // Create an observer instance linked to the callback function
    const observer = new MutationObserver(()=>{
        updateItems();
        //don't flood messages if nothing has changed
        if(items.length !== count){
            count = items.length;
            sendToBackground("imageCount",{imageCount:count});     
        }
    });

    observer.observe(targetNode, config);

}());