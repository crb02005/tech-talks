(function(){
    let tabId = 0;
    const sendToContent = async (tabId, messageType, data=null)=>{
        try {
            return await chrome.tabs.sendMessage(tabId,[
                messageType,
                data
            ]);
        } catch(er){
            // Yum tasty errors, this is a program for demo, in a real program most likely you would not eat errors.
            return null;
        }
    }
    const requestHandlers = {"imageCount":(sender,data)=>{
        console.log(`new image count ${data.imageCount}`);
        chrome.browserAction.setBadgeText({text:data.imageCount.toString()});
    },"requestActiveTab":(sender,data)=>{
        console.log("was asked for a tab");
        return tabId;
    }};

    console.log('adding a badgeClick handler');
    chrome.browserAction.onClicked.addListener((sender)=>{
        sendToContent(sender.id,"badgeClicked",{});
    })

    console.log('adding a tabBackground handler');
    chrome.tabs.onActivated.addListener((activeInfo)=>{
        console.log('Activated a tab background');
        tabId=activeInfo.tabId;
        chrome.browserAction.setBadgeText({text:""});
        sendToContent(activeInfo.tabId,"activatedTab",{});
    })
    console.log('adding a message received from content script handler');
    chrome.runtime.onMessage.addListener((message,sender,respond)=>{
        // didn't add a console log because it would be spammy
        // we have one listener and it routes to the ones in the dictionary above
        const [messageType,data]=message;
        let r= requestHandlers[messageType](sender,data);
        if(respond){
            console.log('respond was passed so calling it');
            respond(r);
        }
    });
}());
