(function(){
    const images = document.getElementById("image-collection");
    const setImageContent = (c)=>{images.textContent=c;};
    const requestHandlers = {"imageCount":(sender,data)=>{
        console.log(`new image count ${data.imageCount}`);
    }};
    const responseHandlers = {"requestActiveTab":(response)=>{
        sendToContent(response,"images",{});
    },"images":(response)=>{
        //console.log("images resp",response);
        setImageContent(response);
    }
    };
    const sendToBackground = async (messageType, data=null)=>{
        console.log("popup sending to background");
        /* This sends a message from the content script to the background script */
         try {
             return await chrome.runtime.sendMessage([
                 messageType,
                 data
             ],(response)=>{
                if(messageType in responseHandlers){
                    responseHandlers[messageType](response);
                 }
             });
         } catch(er){
             console.error("failed to sendToBackground, error:",er);
             return null;
         }
     };
    const sendToContent = async (tabId, messageType, data=null)=>{
        try {
            return await chrome.tabs.sendMessage(tabId,[
                messageType,
                data
            ],(response)=>{
                if(messageType in responseHandlers){
                    responseHandlers[messageType](response);
                 }
            });
        } catch(er){
            // Yum tasty errors, this is a program for demo, in a real program most likely you would not eat errors.
            return null;
        }
    }

    sendToBackground("requestActiveTab",{});
    console.log('adding a message received from content script handler');
    chrome.runtime.onMessage.addListener((message,sender,respond)=>{
        // didn't add a console log because it would be spammy
        // we have one listener and it routes to the ones in the dictionary above
        const [messageType,data]=message;
        let r = requestHandlers[messageType](sender,data);
        if(respond){
            respond(r);
        }
    });
    
}());
