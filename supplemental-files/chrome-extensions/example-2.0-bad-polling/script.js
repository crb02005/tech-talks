(function(){
    let items = [];
    let count = 0;

    const updateItems = ()=>{
        items=document.getElementsByTagName("img");
    }
    const requests = {"badgeClicked":(data)=>{
        // Simple example of something you could do with the badge
        alert("You clicked on the badge");
    }};

    const sendToBackground = async (messageType, data=null)=>{
       /* This sends a message from the content script to the background script */
        try {
            return await chrome.runtime.sendMessage([
                messageType,
                data
            ],(response)=>{
                //in this current pattern we aren't doing anything with the response, we could enhance the pattern to make use of it
                // right now this response is null
                //console.log(response);
            });
        } catch(er){
            console.error("failed to sendToBackground, error:",er);
            return null;
        }
    };

    //Wire up our messages from chrome to our listeners.
    // There are rules on which script can do what so we pass messages.
    chrome.runtime.onMessage.addListener((message) => {
        const [ messageType, data ] = message;
        return requests[messageType](data);
    });

    // Lazy, naive approach, just update every 2 seconds;
    // What happens with multiple tabs?
    setInterval(function(){
        updateItems();
        //don't flood messages if nothing has changed
        if(items.length !== count){
            count = items.length;
            sendToBackground("imageCount",{imageCount:count});     
        }
    },2000);

    observer.observe(targetNode, config);

}());