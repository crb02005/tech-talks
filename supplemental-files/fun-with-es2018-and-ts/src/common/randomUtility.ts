namespace Common.RandomUtility {
    function getRandomInt(max:number):number {
      return 1+Math.floor(Math.random() * Math.floor(max)); // Normal old javascript stuff.
    }
    export function* getRoll(dieCount: number, dieType: number) {
      // https://stackoverflow.com/questions/3895478/does-javascript-have-a-method-like-range-to-generate-a-range-within-the-supp
        for(let i in Array.from(Array(dieCount).keys())/* kind of like the range operator */){

          // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/yield
          yield getRandomInt(dieType); // mmm... iterator
        }
    }
}
