/// <reference path="common/randomUtility.ts" />
let document: any;
namespace App {
  //Ignore the document.writes/ br =)
  //https://www.typescriptlang.org/docs/handbook/release-notes/typescript-2-3.html
  for (let dieRoll of Common.RandomUtility.getRoll(5,6)/* See tsconfig  downlevelIteration true fixes ts "of" */){
    document.write(`You rolled ${dieRoll}<br/>`) // template strings;
    //https://basarat.gitbooks.io/typescript/docs/template-strings.html
  }
  document.write(`Roll finished.`);
}
