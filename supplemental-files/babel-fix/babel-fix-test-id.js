const t = require("@babel/types");
const chalk = require('react-dev-utils/chalk');
console.log(chalk.white("the plugin imported/or required"));

module.exports = function(babel) {
  const { types: t } = babel
  console.log(chalk.white("the plugin was called"));

  return {
    name: "ast-transform", // not required
    visitor: {
      //visit JSXAttributes
      JSXAttribute(path) {
        var node = path.node
        console.log(chalk.yellow("the plugin visitor for JSXAttribute was called"));
        if (node.name.name === "data-testid") {
          console.log(chalk.green("A match was found"));
          //Sort of works, won't show up in browser, but is still in the module
          path.remove();

        }

      }
    }
  }
}

