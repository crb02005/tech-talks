import dice from 'trocar-dice-js';

export default function handler(req, res) {
    res.status(200).json({ result: dice.dice(req.query.id,req.query.sides)  })
  }

  
