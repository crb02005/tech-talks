import react from 'react';
import { useRouter } from "next/router"

const Bar = ()=>{
    const router = useRouter()
    const {
      query: { id },
    } = router
    return <>
        <h1>You've got {id}!</h1>
    </>
}
export default Bar;
