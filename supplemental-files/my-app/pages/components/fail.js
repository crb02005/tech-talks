import styles from './fail.module.css'
const Fail = ()=><><div className={styles.whaley}>
▄██████████████▄▐█▄▄▄▄█▌<br/>
██████▌▄▌▄▐▐▌███▌▀▀██▀▀<br/>
████▄█▌▄▌▄▐▐▌▀███▄▄█▌<br/>
▄▄▄▄▄██████████████<br/>
</div>
<div className="whaley">
▄██████████████▄▐█▄▄▄▄█▌<br/>
██████▌▄▌▄▐▐▌███▌▀▀██▀▀<br/>
████▄█▌▄▌▄▐▐▌▀███▄▄█▌<br/>
▄▄▄▄▄██████████████<br/>
</div>
</>
export default Fail;