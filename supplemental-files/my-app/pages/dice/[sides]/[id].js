import react from 'react';
import dice from 'trocar-dice-js';
import { useRouter } from "next/router"
function getRandomInt(max) {
    return Math.floor(Math.random() * max);
  }
const DieRoller = ()=>{
    const router = useRouter()
    const {
      query: { 
        sides, 
        id 
    }
    } = router
    const result = dice.dice(id,sides);
    return (<>
        <h1>You have elected to roll a die with {sides} sides and you rolled {id} number of times for a result of {result}!</h1>
        <h2>Totally random number {getRandomInt(1000)}.</h2>
    </>
    )
}

export default DieRoller;
