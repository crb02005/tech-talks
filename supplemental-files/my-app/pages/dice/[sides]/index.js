import react from 'react';
import dice from 'trocar-dice-js';
import { useRouter } from "next/router"
const DieRoller = ()=>{
    const router = useRouter()
    const {
      query: { 
        sides
    }
    } = router
    const result = dice.dice(1,sides);
    return (<>
        <h1>You have elected to roll a die with {sides} sides and you rolled 1 time for a result of {result}!</h1>
    </>
    )
}

export default DieRoller;
