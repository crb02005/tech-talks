import react, {useState, useEffect} from 'react';

const Tester = ({})=>{
    const [someField,setSomeField]=useState(1);
    useEffect(()=>{
        console.log("Tester Log Message");
    },[someField]);
    console.log("Will it be client?");
    return <>
        <h1>It Works!</h1><br/>
        <button onClick={()=>setSomeField(someField+1)}>Add 1</button>
        <br/>
        SomeField:'{someField}'
    </>
}
export default Tester;
