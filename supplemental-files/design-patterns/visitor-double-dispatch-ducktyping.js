class Dog{
    constructor(name, breed){
        this._name = name;
        this.breed = breed;
        this.walked = false;
        this.hasEaten = false;
        this.sick = false;
    }
    walk(){
        if(this.walked || this.hasEaten){
            this.sick=true;
        }
        this.walked = true;
    }
    feed(food){
        if(this.hasEaten){
            this.sick =true;
        }
        this.hasEaten = true;
    }
    accept(visitor){
        visitor.visit(this);
    }
    toString(){
        //If you're minifying the JavaScript the name of the constructor will change.
        return `${this._name} - ${this.constructor.name} - ${this.breed} walked? ${this.walked} hasEaten? ${this.hasEaten} - sick: ${this.sick} `;
    }
}

class Cat{
    constructor(name){
        this._name = name;
        this.hasEaten = false;
        this.sick = false;
    }
    feed(food){
        if(food=="fish"){
            if(this.hasEaten){
                this.sick=true;
            }
            this.hasEaten=true;
        }
    }
    //Double dispatch. Showing this for discussion
    // in case you encounter this pattern in the future in another language.

    /* Consider for a moment C# 
    // Visitor could do something like:
    visit(Dog dog)=>dog.feed("kibble");
    visit(Cat cat)=>cat.feed("fish");
    visit(IAnimal animal)=>animal.feed(defaultFood);
    */

    // In Javascript this doesn't make much sense.
    // In Typescript you have to declare the types for the overload 
    // and then implement the switching yourself, not really that
    // helpful except exposing the intended use.

    // So what else could you do with double dispatch in Javascript 
    // since you don't get the typing overloading benefit?
    // the item being visited could do events
    // the item being visited could do something to the visitor itself
    // ??? Profit ???

    accept(visitor){
        visitor.visit(this);
    }
    toString(){
        //If you're minifying the JavaScript the name of the constructor will change.
        return `${this._name} - ${this.constructor.name} - hasEaten? ${this.hasEaten} - sick: ${this.sick}`;
    }
}
class Feeder{
    constructor(food){
        this.food = food;
    }
    visit(feedable){
        feedable.feed(this.food);
    }
}

// BRING ME A BUCKET
class MultiFeeder{
    constructor(foods){
        this.foods = foods;
    }
    visit(feedable){
        for (const food of this.foods) {
            feedable.feed(food);        
        }
    }
}

class BetterMultiFeeder{
    constructor(foods){
        this.foods = foods;
    }
    visit(feedable){
        for (const food of this.foods) {
            if(!feedable.hasEaten){
             feedable.feed(food);        
            }
        }
    }
}
class SmartFeeder{
    
    constructor(foods,defaultFood, dietaryChart){
        this.foods = foods;
        this.dietaryChart = dietaryChart
        const knownItems = Object.keys(dietaryChart);
        this.selectFood=(name)=>knownItems.indexOf(name)>-1 ?dietaryChart[name]: defaultFood; 
    }
    visit(feedable){
        //If you're minifying the JavaScript the name of the constructor will change.
        feedable.feed(this.selectFood(feedable.constructor.name));
    }
}

class Walker{
    visit(walkable){
        //Duck Typing // If it walks (like a duck or just walks in general), then lets take it for a walk.
        if(walkable.walk !== undefined){
            walkable.walk();
        }
    }
}

const menagerie = [
        new Dog("Joe","Basset Hound"),
        new Dog("Frank","Beagle"),
        new Dog("Chet","Pug"),
        new Cat("garbage")// borrowed from the Dwight collection
    ];

const scheduledStaff = [
    //new Feeder("kibble"),
    //new Feeder("fish"),
    //new MultiFeeder(["kibble","fish"])
    //new Walker(),
    //new BetterMultiFeeder(["kibble","fish"]),
    new SmartFeeder(["kibble","fish","tacos"],"kibble",{"Dog":"kibble","Cat":"fish"})
];

function Display(items){
    for(item of items){
        console.log(item.toString());
    }
}

console.log("Before");
Display(menagerie);
for (const staffMember of scheduledStaff){
    for (const animal of menagerie) {
        animal.accept(staffMember);
    }
}
console.log("After");
Display(menagerie);

// Fun Read:
// https://stackoverflow.com/questions/10314338/get-name-of-object-or-class/10314492

