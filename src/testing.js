import React from 'react';
import Presentation from './presentation/presentation';
import About from './about';
import Picture from './picture';
import {GiJesterHat} from 'react-icons/gi';
import {FaMicrosoft,FaCheckCircle} from 'react-icons/fa';
import {IoIosCloseCircleOutline} from 'react-icons/io';
import ExternalLink, {LazyLink} from './external-link';
import ImageMacro from './image-macro';

const Jest = (props)=><ExternalLink href="https://jestjs.io/"> <GiJesterHat/> Jest </ExternalLink>
const CSharp = (props)=><ExternalLink href="https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/language-specification/introduction"><FaMicrosoft/>C#</ExternalLink>

export default ()=><Presentation slides={[
  {caption:"Testing",content:<>
  <div style={{fontSize:"30vh", textAlign:"center"}}>
  <span style={{color:"red"}}><IoIosCloseCircleOutline/></span>
  <span style={{color:"green"}}><FaCheckCircle/></span>
  Refactor
  </div>
  </>},
  {caption:"About Me",content:<About/>},
  {caption:"Is testing scary?",content:<Picture src="content/testing/scary.png"/>},
  {caption:"Why should you test",content:<>
  <ul>
    <li>You want to make sure your code works. </li>
    <li>You want to make sure the product owner is happy with the product.</li>
    <li>You want to make sure you didn’t break something with your new code. </li>
    <li>You want to let others in your codebase and don’t want them to break the world. </li>
    <li>You care about quality. </li>
    <li>All these reasons are good, but quality of life is the most important reason. </li>
  </ul>
  <p>
  Your codebase is where you spend your time. 
  </p>  
  <p>You want to make your job as easy as possible and tests let you do that.
  </p><p>Tests improve development time, you can write code and not fully spin up the entire app if you want to test one thing. 
  With the right test you only have to test part of your code.
  </p>
  </>},
  {caption:"Types of Tests", content:<ul>
  <li>Unit</li>
  <li>Integration</li>
  <li>System</li>
  <li>Sanity</li>
  <li>Smoke</li>
  <li>Interface</li>
  <li>Regression</li>
  <li>Beta Testing</li>
  <li>Coded UI</li>
</ul>},
{caption:"What kind of tests should I write?", content:<>
  <Picture src="content/testing/dwight-office-pyramid.png"></Picture>  

  <a href="https://martinfowler.com/bliki/TestPyramid.html">Test Pyramid - https://martinfowler.com/bliki/TestPyramid.html</a>

</>},
{caption:"What how many tests should I write?", content:<>

<a href="https://martinfowler.com/bliki/TestCoverage.html">Test Coverage - https://martinfowler.com/bliki/TestCoverage.html</a>
<br/>
<Picture src="content/testing/test-all-the-things.png"></Picture>  

</>},
{caption:"Unit Tests", content:<>

<h2>Individual units or components are tested.</h2>
<p>
In <Jest/> you could do something like this:
<br/>
<LazyLink href="https://jestjs.io/docs/en/tutorial-react">React Tutorial </LazyLink>

  </p>
<p>
  In <CSharp/> you could do something like this:
  <ul>
    <li>
    <LazyLink href="https://docs.microsoft.com/en-us/visualstudio/test/unit-test-basics?view=vs-2019">C# Unit Test Basics </LazyLink>
    
    </li>
    <li>
    <LazyLink href="https://docs.microsoft.com/en-us/visualstudio/test/walkthrough-creating-and-running-unit-tests-for-managed-code?view=vs-2019">
    Walkthrough for creating and running unit tests
    </LazyLink>

    </li>
  </ul>
</p>
</>},
{caption:"Integration Tests", content:<>
You are combining things to test. You are finding fault with the interactions between the units.
<h1>Add funny joke about Tekken Combos here</h1>
</>},
{caption:"System Tests", content:<>
You are testing the whole application, not just a couple of combinations. You are evaluating functional and non-functional requirements.
</>},
{caption:"Sanity", content:<>
Sanity Testing is the “show stopper” initial regression testing. These are the mission critical parts of the app you test before doing a full smoke test. If X doesn’t work I shouldn’t test any further.
</>},
{caption:"Smoke", content:<>
You powered on the device and didn’t let out the magic blue smoke. When I was attending my Ham radio’s tech nights they often talked about letting the smoke out. If you wired up something wrong the device would destroy a component and smoke would come out and you’d have some repairs to do. In the software world you are testing the major functionality of your application.
</>}
,
{caption:"Interface", content:<>
This is a type of integration testing which validates the contract you’ve defined.
</>},
{caption:"Regression", content:<>
With this type of testing you are confirming changings to the application doesn’t break the existing features.
</>},
{caption:"Beta Testing", content:<>
Handing the product out to a small population of users to try out the new features.
</>},
{caption:"Coded UI", content:<>
Tests which operate on the user interface and exercise the application the same way the user would.
<LazyLink href="https://en.wikipedia.org/wiki/Selenium_(software)"/>
</>},
{caption:"Red, Green, REFACTOR ", content:<>
<ul>
<li>Basically this is write a failing test.</li>
<li>Write code to make the test pass.</li>
<li>Rewrite the code to make it better.</li>
<li>How do you check if your code works?</li>
</ul>
<h2>Perfection is achieved, not when there is nothing more to add, but when there is nothing left to take away. - Antoine de Saint-Exupery</h2>
</>}
,{caption:"TDD",content:<>
Test Driven Development. See Red, Green, Refactor.

You can’t write untestable code if you write your test first.
</>}
,{caption:"Shift Left",content:<>
You move the testing to the front rather than the end. “Test early and often” - Larry Smith, Dr. Dobbs “Shift-Left Testing”.
</>}
,{caption:"Jest",content:<>
“Jest is a delightful JavaScript Testing Framework with a focus on simplicity.”
</>}
,{caption:"Mocks",content:<>
  
  <li><LazyLink href="https://jestjs.io/docs/en/mock-functions"/></li>
  <li><LazyLink href="https://jestjs.io/docs/en/timer-mocks"/></li>
  <li><LazyLink href="https://jestjs.io/docs/en/manual-mocks"/></li>
  <li><LazyLink href="https://jestjs.io/docs/en/es6-class-mocks"/></li>
</>}
,{caption:"Enzyme",content:<>
“Enzyme is a JavaScript Testing utility for React that makes it easier to test your React Components’ output. You can also manipulate, traverse, and in some ways simulate runtime given the output.”

<LazyLink href="https://airbnb.io/enzyme/"></LazyLink>

<h3>Shallow</h3>
<p>

Renders the component and only direct descendents.</p>
<h3>Mount</h3>
<p>
Renders the component and all descendents.
</p>
</>}
,{caption:"Other things to be aware of",content:<>
“Jasmine is a behavior-driven development framework for testing JavaScript code. It does not depend on any other JavaScript frameworks. It does not require a DOM. And it has a clean, obvious syntax so that you can easily write tests.”
<LazyLink href="https://jasmine.github.io/"></LazyLink>

<ul>
<li><ExternalLink href="https://reactjs.org/docs/testing-recipes.html"> Testing Recipes from React</ExternalLink></li>
<li><ExternalLink href="https://storybook.js.org/docs/testing/react-ui-testing/">Storybook</ExternalLink></li>
<li><ExternalLink href="https://jestjs.io/blog/2016/07/27/jest-14.html">Snapshots</ExternalLink></li>
<li><ExternalLink href="https://jestjs.io/blog/2019/01/25/jest-24-refreshing-polished-typescript-friendly">Typescript</ExternalLink></li>
</ul>

</>}
,{caption:"Image Macros! Now in Text!",
content:<>
<ImageMacro title="Borat thumbs up!"
  top="The tests were broken"
  bottom="So I disabled them"
/>
<ImageMacro title="Roll Safe"
  top="Your code can’t fail unit tests"
  bottom="If you don’t make any unit tests"
/>
<ImageMacro title="Grumpy Cat"
  top="No Tests?"
  bottom="No Code Review"
/>

</>}

,{caption:"Plans",content:<>
<p>
International Software Testing Qualifications Board has a good glossary of testing <ExternalLink href="https://glossary.istqb.org/en/search/">terms</ExternalLink>.
<dl>
  <dt>
Test Plan
  </dt>
  <dd>
  "Documentation describing the test objectives to be achieved and the means and the schedule for achieving them, organized to coordinate testing activities. "
  </dd>
</dl>

<ul>
  <li>JIRA</li>
  <li>TFS</li>

</ul>

</p>

</>},{
  caption:"DEMO",
  content:<>
  
  
  <h3>Live Demo</h3>
  </>},{
  caption:"I don't like these I'll make my own",
  content:<>
  Sure you can write your own framework. I've written my own for <ExternalLink href="https://gitlab.com/crb02005/UnitTest">VB.NET</ExternalLink>, but that was because the free visual studio at the time didn't support testing out of the gate. It does now. I'd use a prebuilt one that has lots of community support instead.

  </>}
  
,{caption:"With Tests you can sleep peacefully",
content:<>
<Picture src="content/testing/sleep.jpg"/>

</>}



]}>




{/*Remove All Images (with force)  */}



  {/* // {caption:"",content:<></>}
  // {caption:"",content:<></>} */}
  
</Presentation>