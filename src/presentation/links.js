import React from 'react';
export default (props)=><>
<ul>
{props.links.map((x,key)=><li><a href={x}>{x}</a></li> )}
</ul>
</>