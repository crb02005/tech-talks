import React from 'react';
export default (props)=><div className="content">
<div className="row justify-content-center align-items-center">
  <div className="col">
    {props.left}
  </div>
  <div className="col">
    {props.right}
  </div>
</div>
</div>