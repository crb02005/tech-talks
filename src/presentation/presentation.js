import React, {useState} from 'react';
import { Carousel, Dropdown } from 'react-bootstrap';
import './presentation.css';
import Header from '../Header';
export default (props)=>{
    const [index, setIndex] = useState(0);
    const [direction, setDirection] = useState(null);
  
    const handleSelect = (selectedIndex, e) => {
      setIndex(selectedIndex);
      setDirection(e.direction);
    };
    if(props.slides[index]===undefined){
        return <>index: {index} slides: {props.slides.length}</>
    }
return <div className="presentation" style={{backgroundColor:"#EEEEEE"}}>
    
    <Header title={props.slides[index].caption}>
    <Dropdown>
        <Dropdown.Toggle  id="dropdown-basic">
         {props.slides[index].caption}  Slides {index+1} of {props.slides.length}
        </Dropdown.Toggle>
        <Dropdown.Menu>
            {props.slides.map((x,key)=>
             <Dropdown.Item key={key} href="#" onSelect={()=>setIndex(key)}>{x.caption}</Dropdown.Item>
            )}
        </Dropdown.Menu>
    </Dropdown>
    </Header>
    <Carousel 
    
    activeIndex={index}
    direction={direction}
    onSelect={handleSelect}
    interval={null}
    >
        {props.slides.map((slide,key)=>
        <Carousel.Item key={key}>
            <div>
            <div className="slide-container">
                <h1>{slide.caption}</h1>
                {slide.content}
            </div></div>
        </Carousel.Item>
        )}
    </Carousel>     </div>}