import React from 'react';
import LiveCodeDemo from './LiveCodeDemo';
import Presentation from './presentation/presentation';
export default (props)=><Presentation slides={[
  {caption:" Roll Your Own Babel Plugin Adventure",content:<>
  <a href="https://babeljs.io/docs/en/" target="_blank" rel="noopener" ><h1>What is Babel?</h1>  </a>

  <h1> Who should view this talk?</h1>
  <p>People interested in:</p>
  <ul>
    <li>a very shallow example of making a plugin for Babel</li>
    <li>The Visitor Design Pattern</li>
    <li>Double Dispatch Enthusiasts</li>
    <li>Pesky JSX attributes that they don't want to see in the browser</li>
    <li>Learning how to squeeze more milage out of React before ejecting</li>
  </ul>
  <h1>Using a Babel Plugin with React</h1>
  <hr/>
  <h2>If you want to eject turn to page <a href="https://create-react-app.dev/docs/available-scripts/">Available Scripts</a></h2>
  <h2>If you want to don't want to eject turn to page <a href="https://github.com/timarney/react-app-rewired"> React App Rewired</a>....</h2>
  </>},
   {caption:"Things To Know before building the Plugin  Visitor",content:<>
   <br/>
   <h2>Babel is a Transpiler, what is a Transpiler?</h2>
   <p>
     The input is code. The output is code.
   </p>


  <h2> Visitor </h2>  
  <p>Gang of Four 
      "Represent[ing] an operation to be performed on elements of an 
      object structure. Visitor lets you define a new operation without 
      changing the classes of the elements on which it operates."</p>
  <p>Logic is kept in the vistor</p>
  <p>Receiving object, must accept the visitor object and provide access</p>

<LiveCodeDemo location="supplemental-files/design-patterns/visitor-double-dispatch-ducktyping.js"/>
<br/>

  <strong>If want more info on the Visitor Pattern turn to page </strong>  <a href="https://en.wikipedia.org/wiki/Visitor_pattern">https://en.wikipedia.org/wiki/Visitor_pattern</a>
  <br/>
   </>},
   {caption:"Things To Know before Building the Plugin AST Trees",content:<>
    <h2>Operations</h2>
    <p>
      Unary vs Binary: Increment(foo) vs Add(a,b)
    </p>
    <h2>What are Abstract Syntax Trees?</h2>
    <p>
      <a href="https://en.wikipedia.org/wiki/Abstract_syntax_tree">Abstract Syntax Tree</a>
      </p><p>
      "Abstract syntax trees are also used in program analysis and program transformation systems. " - The article linked above.
      <br/>
      The <strong>program transformation</strong> in our case it will be the Babel Transpiler
    </p>
   </>},
    {caption:" Building The Plugin - Eject",content:<>
    <h1>Documentation</h1>
      <a href="https://github.com/jamiebuilds/babel-handbook/blob/master/translations/en/plugin-handbook.md">How to Build A Plugin</a>
    <p>Stop talking and start showing:</p>    
    <LiveCodeDemo location="supplemental-files/babel-fix/"/>
    <h1>Lessons Learned:</h1>
    <ul>
      <li>React is sneaky, and doesn't always recompile</li>
      <li>Well, I'll just delete the build folder, that will fix it. It doesn't</li>
      <li>React isn't exactly the cuplrit, they just abstract and hide complexity, it is Babel/Webpack.... remove the node_modules/.cache</li>
      <li>React is sneaky. Searching for the node we deleted in the build directory you can still find it, but when you run the app... It isn't there in the html</li>
    </ul>
    </>},
    {caption: "Building The Plugin - Rewire",content:<>
    
    <code>
    npm install react-app-rewired --save-dev
    </code>
    <br/>
    Create config-overrides.js
    <br/>
    Swap out the react-scripts for react-app-rewired for everything but eject
    <LiveCodeDemo location="supplemental-files/babel-fix-no-eject/"/>
    </>

    }
]} />


  {/* // {caption:"",content:<></>}
  // {caption:"",content:<></>} */}
