import React from 'react';
import './picture.css';
export default (props) => <div className="picture-container">
    {props.fillbackground ? <img className="picture picture-fill" alt={props.src} src={props.src} />
        : <img className="picture" src={props.src} alt={props.src} />
    }
</div>