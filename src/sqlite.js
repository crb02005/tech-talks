import React from 'react';
import Presentation from './presentation/presentation';
import Links from './presentation/links';
import About from './about';
import Picture from './picture';
import SlideTwoCol from './presentation/slide-two-col';
import {FaDatabase,FaChrome,FaFirefox,FaSadCry} from 'react-icons/fa'; 
import {GiClown} from 'react-icons/gi';
import { LazyLink } from './external-link';
export default (props)=><Presentation slides={[
  {caption:"SQLite - The Little (DB) Engine That Could",content:<SlideTwoCol 
  left={<Picture src="content/sqlite/sqlite.png" fillbackground={true}/>}
  right={<>
      <h2>
    A Mini-Walkthrough of SQLite using C#
    </h2></>
  }>
  </SlideTwoCol>},
  {caption:"About Me",content:<About/>},
  {caption:<><FaDatabase/>SQL</>,content:null},
  {caption:"Frequently Used SQL",content:<Picture src="content/sqlite/common_sql_statements.png"/>},
  {caption:"Great intro into the World of Databases",content:<SlideTwoCol 
  left={<Picture src="content/sqlite/manga.png"/>} right={<h1>
  The Manga Guide to Databases Paperback – Illustrated, January 15, 2009
  </h1>}/>},
  {caption:<><FaDatabase/>SQLite?</>,content:<></>},
  {caption:"What uses SQLite?",content:<>
  <a href="https://sqlite.org/mostdeployed.html">https://sqlite.org/mostdeployed.html</a>
  <ul>
  <li>Every Android device </li>
  <li>Every iPhone and iOS device</li> 
  <li>Every Mac </li>
  <li>Every Windows 10 machine </li>
  <li>Every Firefox, Chrome, and Safari web browser </li>
  <li>Every instance of Skype </li>
  <li>Every instance of iTunes </li>
  <li>Every Dropbox client </li>
  <li>Every TurboTax and QuickBooks</li> 
  <li>PHP and Python </li>
  <li>Most television sets and set-top cable boxes </li>
  <li>Most automotive multimedia systems </li>
  <li>Countless millions of other applications</li> 
  </ul></>},
  {caption:"",content:<>
  <h3><FaFirefox/> Firefox </h3>
  <Picture src="content/sqlite/firefox.png"/><br/>
  Windows Location:
  C:\Users\%USERNAME%\AppData\Roaming\Mozilla\Firefox\Profiles
  <br/>
  Where I Downloaded Firefox Source:
  <a href="file:\\C:\source\firefox">file:\\C:\source\firefox</a>

  </>},
  {caption:<><FaChrome/>Chromium</>,content:<>
  <br/>
  Source Code:
  <br/>
<a href="https://cs.chromium.org/chromium/src/net/extras/sqlite/">https://cs.chromium.org/chromium/src/net/extras/sqlite/</a>
<br/>

Connection:
<br/>
<a href="https://cs.chromium.org/chromium/src/sql/connection.h?l=112">https://cs.chromium.org/chromium/src/sql/connection.h?l=112</a>
<br/>

Local DBs on Windows:
<br/>
C:\Users\%USERNAME%\AppData\Local\Google\Chrome\User Data\Default\databases 

  </>},
  {caption:<><FaSadCry/><GiClown/>(Sad Clown)</>,content:<>
    Edge (prior to chrome conversion) didn’t use Sqlite:
<a href="https://en.wikipedia.org/wiki/Extensible_Storage_Engine">ESE database file</a>

  
  </>},
  {caption:"Stop talking and show me the code"}
  ,
  {caption:"Sqlite from Scratch",content:<>
  Individual vs amalgamation
  <br/>
  With Windows use: MSVC
  <br/>
  <Picture src="content/sqlite/sqlite_from_scratch.png"/>
  </>}
  ,
  {caption:"Sqlite from Scratch Cont.",content:<>
  <Picture src="content/sqlite/sqlite_from_scratch_2.png"/>
  </>}
  ,
  {caption:"Custom Method",content:<>
  <h1>Let's Add An Extension</h1>
  <Picture src="content/sqlite/sqlite_from_scratch_3.png"/>
  </>},
  {caption:"Why can’t I add a reference to this in .NET?",content:<>
  <h2>
<ul>
  <li>
    <a href="https://www.codeproject.com/articles/14180/using-unmanaged-c-libraries-dlls-in-net-applicatio">https://www.codeproject.com/articles/14180/using-unmanaged-c-libraries-dlls-in-net-applicatio</a>
  </li>
  <li>
    <a href="https://stackoverflow.com/questions/3563870/difference-between-managed-and-unmanaged">https://stackoverflow.com/questions/3563870/difference-between-managed-and-unmanaged</a>
  </li>
</ul></h2>
  </>}
  ,
   {caption:"NuGet to the rescue"},

   {caption:"System.Data.SQLite",content:<>
<a href="http://system.data.sqlite.org/index.html/doc/trunk/www/index.wiki">http://system.data.sqlite.org/index.html/doc/trunk/www/index.wiki</a>
<Picture src="content/sqlite/mssqlite.png"></Picture>
   </>},
  {caption:"Install-Package System.Data.SQLite"}  ,
  {caption:"Sqlite Nuget",content:<>
  <Picture src="content/sqlite/nuget_1.png"/>
  </>},
 {caption:"Sqlite Nuget",content:<>
 <Picture src="content/sqlite/nuget_2.png"/>
 </>}
,
{caption:"Sqlite Nuget",content:<>
<Picture src="content/sqlite/nuget_3.png"/>
</>}
,{caption:"SQLite C#  System.Data.Sqlite Example"}
,{caption:"SQLite Browser",content:<Links  links={["http://sqlitebrowser.org/",
"https://github.com/sqlitebrowser/sqlitebrowser"
]}/>}
,{caption:<LazyLink href="https://sqlite.org/whentouse.html"/>}
,
{caption:"ACID",content:<>
<a href="https://blog.sqlauthority.com/2007/12/09/sql-server-acid-atomicity-consistency-isolation-durability/">https://blog.sqlauthority.com/2007/12/09/sql-server-acid-atomicity-consistency-isolation-durability/</a><br/>
<Picture src="content/sqlite/acid.png"/>
</>},
{caption:"A in ACID",content:<Links title="A in ACID" links={[
"https://www.sqlite.org/atomiccommit.html"
]}/>},
{caption:"I in ACID", content:<>
<h3>
<p>
  Transactions in SQLite are SERIALIZABLE. 
  </p><p>
Changes made in one database connection are invisible to all other database connections prior to commit. 
</p><p>
A query sees all changes that are completed on the same database connection prior to the start of the query, regardless of whether or not those changes have been committed. 
</p></h3>
</>},
{caption:"I in ACID Cont.", content:<>
<h3>
<p>
If changes occur on the same database connection after a query starts running but before the query completes, then the query might return a changed row more than once, or it might return a row that was previously deleted. 
</p><p>
For the purposes of the previous four items, two database connections that use the same shared cache and which enable PRAGMA read_uncommitted are considered to be the same database connection, not separate database connections. 
</p>
</h3>
</>},
{caption:"What Can You Do",content:<>
<ul>
<li><a href="https://sqlite.org/eqp.html">https://sqlite.org/eqp.html - Explain a Query</a></li>
<li><a href="https://sqlite.org/backup.html">https://sqlite.org/backup.html - Backup a live database</a></li>
<li><a href="https://sqlite.org/zipfile.html">https://sqlite.org/zipfile.html - Handle Zip files</a></li>
<li><a href="https://sqlite.org/download.html">https://sqlite.org/download.html - Update the Source code</a></li>
<li><a href="https://sqlite.org/threadsafe.html">https://sqlite.org/threadsafe.html - Threading!</a></li>
</ul>
With Extensions
<ul>
<li><a href="https://www.sqlite.org/json1.html">https://www.sqlite.org/json1.html - JSON SQL functions for creating, parsing, and querying JSON content.</a></li>
<li><a href="https://www.sqlite.org/fts5.html">https://www.sqlite.org/fts5.html - A description of the SQLite Full Text Search (FTS5) extension.</a></li>
</ul>
Other  tricks:
<ul>
<li><a href="http://www.sqlite.org/c3ref/stmt.html">http://www.sqlite.org/c3ref/stmt.html - Compile a SQL statement!</a></li>
<li><a href="https://www.sqlite.org/howtocompile.html#building_a_windows_dll"> https://www.sqlite.org/howtocompile.html#building_a_windows_dll - Optimizing the engine for Size or Speed</a></li>
<li><a href="https://stackoverflow.com/questions/784173/what-are-the-performance-characteristics-of-sqlite-with-very-large-database-file?rq=1">https://stackoverflow.com/questions/784173/what-are-the-performance-characteristics-of-sqlite-with-very-large-database-file?rq=1</a>Use a 160GB database</li>
</ul>
</>},
{caption:"Things that might take effort",content:<>
Things that might take effort:
<ul>
  <li>Replication</li>
  <li><a href="http://litesync.io/">http://litesync.io/</a></li>
  <li><a href="https://github.com/rqlite/rqlite">rqlite</a></li>
  <li><a href="http://www.scs.stanford.edu/17au-cs244b/labs/projects/muindi_stern.pdf">http://www.scs.stanford.edu/17au-cs244b/labs/projects/muindi_stern.pdf</a></li>
</ul>

</>},
{caption:"What you might have expected but is missing",content:<ul>
<li>
  Client / Server
</li>
<li>
  Installation
</li>
<li>
  Starting and Stopping Services
</li>
<li>
  Configuration (SQLite is called zero-conf)
</li>
</ul>},
{caption:"Security",content: <Picture src="content/sqlite/security.png"/>},
{caption:"User Security",content:<Links links={[
"https://docs.oracle.com/cd/E11882_01/network.112/e10744/concepts.htm#DBIMI152"
,"https://docs.microsoft.com/en-us/sql/relational-databases/security/authentication-access/create-a-database-user"
,"https://technet.microsoft.com/en-us/security/gg483744.aspx"
,"https://docs.mongodb.com/manual/security/"
,"https://stackoverflow.com/questions/11044261/sqlite-db-security"
,"https://stackoverflow.com/questions/2493331/what-are-the-best-practices-for-sqlite-on-android"

]}></Links>},
{caption:"Redis & SQLite",content:<Links links={[
"https://www.sqlite.org/cvstrac/wiki?p=KeyValueDatabase"
,"https://stackoverflow.com/questions/11216647/why-is-sqlite-faster-than-redis-in-this-simple-benchmark"    
]}></Links>},
{caption:"Backups",content:<Links links={[
"https://www.sqlite.org/howtocorrupt.html"
,"https://sqlite.org/backup.html"
,"https://sqlite.org/c3ref/backup_finish.html"
  
]}></Links>},
{caption:"Powershell",content:<>
<ul>
<li><a href="https://system.data.sqlite.org/index.html/doc/trunk/www/downloads.wiki">https://system.data.sqlite.org/index.html/doc/trunk/www/downloads.wiki</a></li>
<li>Download and install from Github</li>
{/* 2012 prevent bogus warning */}
<li>Add-Type -Path "C:\Program Files\System.Data.SQLite\{"2012"}\bin\System.Data.SQLite.dll“</li>
</ul>
</>}    
,{caption:"Selecting Data Demo 1",content:<></>}
,{caption:"Selecting Data Grid Demo 1.1",content:<></>}
,{caption:"Inserting Data Demo 2.1",content:<></>}
,{caption:"Inserting Data -CSV Demo 2.2",content:<></>}
,{caption:"Inserting Data -CSV Demo Native Support",content:<>
.import "C:\\Users\\crb02\\Documents\\Code\\donet_sqlite_talk\\source\\books.csv" books
</>}
,{caption:"Inserting Data –XML Demo 2.3",content:<></>}
,{caption:"Questions and Answers",content:<></>}
,{caption:"Further Reading",content:<Links
links={
["https://sqlite.org/docs.html"
,"https://www.nuget.org/packages/System.Data.SQLite"
,"https://developer.android.com/reference/android/database/sqlite/package-summary.html"
,"https://sqlite.org/famous.html"
,"http://blog.maskalik.com/asp-net/sqlite-simple-database-with-dapper/"
,"http://www.sqlalchemy.org/"
,"https://sqlite.org/queryplanner-ng.html"
,"https://en.wikipedia.org/wiki/Fossil_(software)"
,"https://blog.sqlauthority.com/"
,"https://social.technet.microsoft.com/wiki/contents/articles/30562.powershell-accessing-sqlite-databases.aspx"
,"https://medium.com/@ratulbasak93/backing-up-sqlite3-db-to-aws-s3-962b99744065"
]
}
>
</Links>},
{caption:"Speaker Note",content:<ul><li>
<a href="content/sqlite/talk.ps1">content/sqlite/talk.ps1</a>  </li></ul>}
]}>


  {/* // {caption:"",content:<></>}
  // {caption:"",content:<></>} */}
  
</Presentation>