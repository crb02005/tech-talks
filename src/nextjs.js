import React from "react"
import Presentation from "./presentation/presentation";
require('./nextjs.css');

const CompareRow = ({ name, left, right }) => {
  return <tr>
    <td>
      {name}:
    </td>
    <td>
      {right}
    </td>
    <td>
      {left}
    </td>
  </tr>
}
const NextJS = () => {
  return <Presentation slides={[
    {
      caption: "NextJS", content: <>
        <table className="comparison">
          <thead>
            <tr>
              <th>Comparison</th>
              <th>Client Side React</th>
              <th>NextJS</th>
            </tr>
          </thead>
          <tbody>
            <CompareRow
              name="Made By"
              right="Facebook"
              left={<a href="https://vercel.com/about/rauchg" target="_blank" rel="noopener noreferrer" >Vercel</a>}
            />
            <CompareRow
              name="Server Requirements"
              right={<>Compile to static files in production build to be served by any static content server</>}
              left="Uses Vercel's server, or custom server.js node server, not static"
            />
            <CompareRow
              name="Boundaries"
              right="Clear boundaries around client, and server technologies "
              left="Client and Server are tightly woven together."
            />
            <CompareRow
              name="Routing"
              right="Unopinionated routing, you pick the one you want."
              left={<a href="https://nextjs.org/docs/routing/introduction" target="_blank" rel="noopener noreferrer" >Opinionated routing</a>}
            />
            <CompareRow
              name="Single Responsibility"
              right="Closer to a Single Responsibility"
              left={"No, Kitchen Sink, Opinionated Framework"}
            />
            <CompareRow
              name="Community"
              right="Large, many active discords, many resources"
              left="Smaller, but active discord"
            />
            <CompareRow
              name="Documentation"
              right="Lots, and from many sources"
              left="mainly from their own website"
            />
            <CompareRow
              name="Popular"
              right={<a href="https://insights.stackoverflow.com/survey/2020#technology-web-frameworks" target="_blank" rel="noopener noreferrer" >Popular</a>}
              left="Not as Popular as its components: React, NodeJS, Express"
            />
            <CompareRow
              name="SEO"
              right={"Needs additional libraries to help non-javascript parsers."}
              left={"Content can be generated server side so web spiders so people can find your content"}
            />
            <CompareRow
              name="Learning Curve"
              right="Plenty to learn from React with a foundation of Javascript, Babel, WebPack"
              left="Plenty to learn from NextJS and React, but also NodeJS, Express, and Vercel's Server if you aren't rolling your own"
            />
            <CompareRow
              name="Initial Setup"
              right="Install react, pick a router, figure out which backend you are talking to, etc"
              left="Install NextJS, work within its framework"
            />
          </tbody>
        </table>
      </>
    }, {
      caption: "Getting Started", content: <>
        <a href="https://nextjs.org/docs/getting-started">Documentation For Getting Started</a>
        <p>We are going with vanilla javascript in our demo, but know that it supports TypeScript if you want LARP that JavaScript supports more types than:</p><ul>
          <li>string</li>
          <li>number</li>
          <li>boolean</li>
          <li>null</li>
          <li>undefined</li>
          <li>symbol</li>
        </ul>
        <h2>Create the app</h2>
        <pre>
          npx create-next-app
        </pre>
        <p>Type a nice name, I left the default wonderfully original name of my-app </p>
        <pre>
          cd my-app
          npm run dev
        </pre>
        <p><strong>
          Lets take a moment to talk about dark patterns, we shall call this one: "Telemetry"</strong>
        </p>
        <cite>Attention: Next.js now collects completely anonymous telemetry regarding usage.
This information is used to shape Next.js' roadmap and prioritize features.
You can learn more, including how to opt-out if you'd not like to participate in this anonymous program, by visiting the following URL:
</cite><br/>
<a href="https://nextjs.org/telemetry" target="_blank" rel="noopener noreferrer">https://nextjs.org/telemetry</a>
<p>
  TL;DR; You may also opt-out by setting an environment variable: NEXT_TELEMETRY_DISABLED=1.
</p>
      </>
    },{caption:"DEMO TIME",content:<>
    <ul>
      <li><a href="http://localhost:3000/demo1"  target="_blank" rel="noopener noreferrer">Demo 0 - Does it open?</a></li>
      <li>Demo 1 - Our first Guessing Game, Is it Server Or Client!</li>
    </ul>
    
    </>},{caption:"Routing",content:<>
    <ul>
      <li><a href="http://localhost:3000/demo2"  target="_blank" rel="noopener noreferrer">Demo 2 - Does it open? Show index.js automatically routes in a folder</a></li>
      <li><a href="http://localhost:3000/demo2/demo3"  target="_blank" rel="noopener noreferrer">Demo 3 - Making Routes</a></li>
      <li><a href="http://localhost:3000/dice/6/" target="_blank" rel="noopener noreferrer"> Demo 4.0 - Nested Routes</a></li>
      <li><a href="http://localhost:3000/dice/6/3" target="_blank" rel="noopener noreferrer"> Demo 4.1 - Nested Routes</a></li>
    </ul>
    </>},{caption:"Rendering",content:<>
    <h1 style={{color:"white", backgroundColor:"red"}}>BY DEFAULT EVERY PAGE IS PRERENDERED</h1>
    <p>Static (Build) Vs Server Side Rendering (At Request)</p>
    <h2>Static Generation:</h2>
    <p>The HTML is generated at build time and will be reused on each request.</p>
    <h2>Server-side Rendering:</h2>
    <p>The HTML is generated on each request.</p>
    <a href="https://nextjs.org/docs/basic-features/pages#pre-rendering">More information from NextJS</a>
    </>},{
      caption:"Styling!",
      content:<>
        <ul>
          <li>Demo Untitled - CSS - global with styles/globals.css</li>
          <li><a href="https://nextjs.org/docs/basic-features/built-in-css-support" target="_blank" rel="noopener noreferrer" >From node_modules import in _app.js</a></li>
          <li><a href="http://localhost:3000/demo6" target="_blank" rel="noopener noreferrer">Demo 6 (A tale of two whales!)</a> - CSS - component level with /components/[Your Component Name].module.css</li>
          <li>SASS also works, left as an exercise for the reader</li>
        </ul>
      </>
    },{caption:"Images",
  content:<>
  <strong>"Instead of optimizing images at build time, Next.js optimizes images on-demand, as users request them. Unlike static site generators and static-only solutions, your build times aren't increased, whether shipping 10 images or 10 million images."</strong>
  <p><a href="https://nextjs.org/docs/basic-features/image-optimization">Image optimization</a></p>
  <ul>
    <li><a href="http://localhost:3000/demo7" target="_blank" rel="noopener noreferrer">Demo 7 - Image</a></li>
  </ul>
  </>},{
      caption:"SWR",
      content:<>
      <h1>stale-while-revalidate</h1>
      <p>Shows stale data until data is available then shows the new data.</p>
      <a href="https://swr.vercel.app/">SWR on Vercel</a><br/>
      <h2>
        Lets have a discussion about this!
      </h2>
      </>
    },{caption:"APIs",content:<><ul>
      <li><a href="http://localhost:3000/api/hello">Demo 8</a> 
        </li>
        <li><a href="http://localhost:3000/api/dice/10/2">Demo 9</a>
        </li>
      </ul></>},{caption:"Final Thoughts",content:<>
      <h1>Lots of Features, Lots of Options, Is it the right choice?</h1>
      <h2>Discussion when to use? when not to use?</h2>
      <h3>Some possible scenarios to cover if no one has any</h3>
      <ul>
        <li>Blog or CMS? Use/ Not Use</li>
        <li>Corporate Web App with few users, and another backing technology like Java, or DotNet Core serving Apis?</li>
        <li>A cloud native application with defined hosting other than Vercel?</li>
        <li>An Image Macro creation tool?</li>
      </ul> 
      </>}]}>
  </Presentation>
}
export default NextJS;
