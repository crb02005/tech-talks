import React from 'react';
import About from './about';
import Picture from './picture';
import Presentation from './presentation/presentation';
import PackageManagers from './slides/package-managers';
export default (props)=><Presentation slides={[
  {caption:"Introduction: The Presenter",content:<About/>},
  {caption:"Package Management - History & Background Knowledge",content:<PackageManagers/>},
  {caption:"Linux Branches",content:<a 
  href="https://upload.wikimedia.org/wikipedia/commons/1/1b/Linux_Distribution_Timeline.svg" 
  target="_blank" rel="noopener noreferrer">
    <Picture
   src="https://upload.wikimedia.org/wikipedia/commons/1/1b/Linux_Distribution_Timeline.svg"/></a>
},
{caption:"Installation",content:<>

<h2>Command 'npm' not found, but can be installed with:</h2>

<p>sudo apt install npm</p>

<h2>What is this going to do to my machine?</h2>
<p class="fragment" data-fragment-index="1">apt-cache depends npm > dependency.txt</p>


</>},
{caption:"Node Basics",content:<>
  <h2>How do you find out what file runs when you type it?</h2>
              <p class="fragment" data-fragment-index="1">which</p>
              <p class="fragment" data-fragment-index="2">which node</p>

  <h2>How do you find out what version you are running?</h2>
              <p class="fragment" data-fragment-index="1">node --version</p>
	<h2>What is the REPL?</h2>
      <p class="fragment" data-fragment-index="1">Read Evaluate Print Loop</p>

  <h2>What commands can I run?</h2>
							<p class="fragment" data-fragment-index="1">JavaScript!!</p>
							<p class="fragment" data-fragment-index="2">.help</p>
							<p class="fragment" data-fragment-index="2">.exit</p>

</>},
{caption:"NPM Basics - Packages",content:<>
							
							<p class="fragment" data-fragment-index="1">Locally</p>
							<p class="fragment" data-fragment-index="2">Globally</p>
</>},
{caption:"NPM Basics - Installing globally without super user permissions",content:<>
							<p class="fragment" data-fragment-index="1">Default Global Location: <a rel="noopener noreferrer" href="https://refspecs.linuxfoundation.org/FHS_3.0/fhs/ch04s09.html" target="_blank"> /usr/local</a></p>
							<p class="fragment" data-fragment-index="2">Who owns it?</p>
							<p class="fragment" data-fragment-index="3">ls -l /usr/local</p>
							<p class="fragment" data-fragment-index="4">What about home?</p>
							<p class="fragment" data-fragment-index="5">ls -l /home/</p>
							<p class="fragment" data-fragment-index="6">mkdir ~/.node_global</p>
							<p class="fragment" data-fragment-index="7">Why the .?</p>
							<p class="fragment" data-fragment-index="8">. = hidden on linux see: ls vs ls -A</p>
</>},

{caption:"NPM Basics - Why is /usr/local the default?",content:<>
<h1>							How to change the default global install directory?</h1>
							<p class="fragment" data-fragment-index="1">npm config set prefix ~/.node_global</p>
							<p class="fragment" data-fragment-index="2">Installing NPM into the user level permission folder</p>
							<p class="fragment" data-fragment-index="2">npm install npm --global</p>
							<p class="fragment" data-fragment-index="3">Fix the Path in .bashrc</p>
							<p class="fragment" data-fragment-index="3">export PATH="$HOME/.node_global/bin:$PATH"</p>
</>},

{caption:"NPM Basics - How to change the default global install directory?",content:<>
							<p class="fragment" data-fragment-index="1">npm config set prefix ~/.node_global</p>
							<p class="fragment" data-fragment-index="2">Installing NPM into the user level permission folder</p>
							<p class="fragment" data-fragment-index="2">npm install npm --global</p>
							<p class="fragment" data-fragment-index="3">Fix the Path in .bashrc</p>
							<p class="fragment" data-fragment-index="3">export PATH="$HOME/.node_global/bin:$PATH"</p>
</>},
{caption:"NPM Commands - Version",content:<>

<h1>How do you find out what version you are running?</h1>
							<p class="fragment" data-fragment-index="1">npm --v</p>
							<p class="fragment" data-fragment-index="2">The NPM version and the Node version are unrelated.</p>

</>},
{caption:"NPM Commands - List",
content:<>
<h1>How do we know what is globally installed?</h1>
							<p class="fragment" data-fragment-index="1">npm list --global</p>
							<p class="fragment" data-fragment-index="2">Can we flatten it?</p>
							<p class="fragment" data-fragment-index="3">npm list --global --depth 0</p>
							<p class="fragment" data-fragment-index="4">Can we learn more?</p>
							<p class="fragment" data-fragment-index="5">npm help list</p>
							<p class="fragment" data-fragment-index="6">How about a JSON file?</p>
							<p class="fragment" data-fragment-index="7">npm list -g --json true</p>

</>},
{caption:"NPM Commands - Install",content:<>
<h2>Getting started with a folder:</h2>
							<p>mkdir demo</p>
							<p>cd demo</p>
							<p>npm init</p>
<h2>Installing a Package</h2>
							<p class="fragment" data-fragment-index="1">
								npm install lodash
							</p>
							<p class="fragment" data-fragment-index="2">
								Seeing what happened:
								cat package.json
							</p>
							<p class="fragment" data-fragment-index="3">
								npm install lodash --save
							</p>
							<p class="fragment" data-fragment-index="4">
								cat package.json									
							</p>
							<p class="fragment" data-fragment-index="5">
								Install everything in the package.json					
							</p>
							<p class="fragment" data-fragment-index="6">
								npm i 					
							</p>
</>}					,
{caption:"NPM Commands - Search",content:<>npm search jquery</>}
,
{caption:"NPM Commands - View",content:<>

<h2>npm view jquery</h2>
							<p class="fragment" data-fragment-index="1">
								This doesn't show everything. Lets add the --json flag to get more information
							</p>

</>},
{caption:"What version is installed?",content:<>
<p class="fragment" data-fragment-index="1">
								npm view lodash version
							</p>
							<p class="fragment" data-fragment-index="2">
								What versions are available?
							</p>
							<p class="fragment" data-fragment-index="3">
								npm view lodash versions
							</p>
</>},
{caption:"NPM Commands - Remove",content:<>
<h2>npm uninstall lodash --save</h2>
<p class="fragment" data-fragment-index="1">
cat package.json
</p></>},
{caption:"NPM Commands - Update",content:<>
<h2>Let's get an old version:</h2>
								npm install lodash@4.16.0 --save
								<p class="fragment" data-fragment-index="1">
									npm outdated
								</p>
								<p class="fragment" data-fragment-index="2">
								npm update lodash --save
								</p>
								<p class="fragment" data-fragment-index="3">
									Let's make sure everything is up to date
								</p>
								<p class="fragment" data-fragment-index="4">
									npm outdated
								</p>
</>},
{caption:"NPM Commands - Cache",content:<>npm cache clean</>},
{caption:"NPM package lock",content:<>

I've got a package.json but I tried my project later and installed and it doesn't work? 
								
								Well that was because someone didn't version things properly.

								<br/>
								Major.Minor.Patch
								<br/>

								<ul>
								<li>
									Creates two "sources of truth"
								</li>
								<li>
									Use package-lock. It helps maintain consistency and dependency compatiblity.
								</li>
								<li>
									Don't _ignore_ it. Check it in.
								</li>
								<li>
									It is higher priority than package.json
								</li>
								<li>
									_npm install_ should regen
								</li>
								<li>
									Package Builders? Use Semantic Versioning. This is out of scope for today's talk but see:
									<p>
										<a href="https://semver.org/">semver</a>
									</p>
								</li>
								<li>
										https://github.com/npm/npm/pull/17508
								</li>
							</ul>

</>},
{caption:"Making your own Package",content:<>
							<a href="https://docs.npmjs.com/packages-and-modules/contributing-packages-to-the-registry">https://docs.npmjs.com/packages-and-modules/contributing-packages-to-the-registry</a>
</>}

							

]}>



  {/* // {caption:"",content:<></>}
  // {caption:"",content:<></>} */}
  
</Presentation>