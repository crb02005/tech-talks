import React from 'react';
//No Reverse Tab Nabbing
const Link = (props)=><a href={props.href} target="_blank" rel="noopener noreferrer">{props.children}</a>
export const LazyLink = (props)=><Link href={props.href}>{props.href} {props.children? <>- {props.children}</>:null} </Link>

export default Link;