import React from 'react';
import About from '../about';
import Presentation from '../presentation/presentation';
import { Link } from 'react-router-dom';

export default (props)=><Presentation slides={[
{caption:"About",content:<About/>},
{caption:"What is a reducer?",content:<>
                                <p class="fragment" data-fragment-index="1">
                                    <a href="https://github.com/mdn/browser-compat-data/blob/master/javascript/builtins/Array.json">array.reduce</a> works in most browsers.
                                </p>
                                <p class="fragment" data-fragment-index="1">
                                    It is a function that lives on an array.
                                </p>
                                <p class="fragment" data-fragment-index="1">

                                    It takes four parameters:
                                    <ul class="fragment" >
                                            <li class="fragment" >Accumulator (acc)</li>
                                            <li class="fragment" >Current Value (cur)</li>
                                            <li class="fragment" >Current Index (idx)</li>
                                            <li class="fragment" >Source Array (src)</li>
                                    </ul>
                                </p>

</>},
{caption:"Manual Reducer Example",content:<Link to="/reducers/manual-example" className="card-link">Hand Rolled Reducer to Demonstration</Link>},
{caption:"What can I do with it?",content:<ul>
    <li>
    <a href="https://reactjs.org/docs/hooks-reference.html#usereducer">https://reactjs.org/docs/hooks-reference.html#usereducer</a>

    </li>
    <li>
        <Link to="/reducers/dispatch-example" className="card-link">Dispatch Example</Link> 

    </li>
    <li>
        <Link to="/reducers/summary-example" className="card-link">Summary Example</Link> 

    </li>
</ul>}
]}>


</Presentation>