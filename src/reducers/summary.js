import React from 'react';


const add = (operand1, operand2) => operand1 + operand2;
const bills = [{
  "name":"rent",
  "balance":2000
},{
  "name":"netflix",
  "balance":25
},{
  "name":"amazon",
  "balance":195
},{
  "name":"humblebundle",
  "balance":259
}]

export default ()=> {

  return (
    <div className="App">
      <header className="App-header">
      </header>
      <div className="container-fluid">
        <ul>
          {bills.map((bill,idx)=><li key={idx}>{bill.name} - {bill.balance}</li> )}
        </ul>
        Total: {bills.map((bill)=>bill.balance).reduce(add)}
      </div>
    </div>
  );
}