import React, {useState} from 'react';
import {FaAngleRight,FaAngleLeft} from 'react-icons/fa';
import Button from 'react-bootstrap/Button';

const Param = ({ index, name, value }) => {
 return <div className="row" >
    <div className="float-left">Param {index}</div>
    <div style={{"width":"auto"}} className="text-center float-left ">
      <div>{name}</div>
      <div className="border border-primary px-2 py-2">{value}</div>
    </div>
  </div>
}

const ExampleStep=({stepNumber,description,parameters,parameterKeys})=>{
return <>
<div>
  <div>
    {description}
  </div>
</div>
  { parameters && Object.keys(parameters).length ?
  parameterKeys.map((parameter, idx) =>  
    <Param key={idx}  index={idx+1} name={parameter} value={parameters[parameter]} />
  ):null}
</>
}

const ReduceExample=({arrayValues, originalValue, func})=>{
  const [currentStep, setCurrentStep] = useState(0);

  const Example = {
    name: "Reduce",
    description: "",
    parameters: ["acc", "cur", "idx", "src"],
    steps: []
  }
  const original = JSON.stringify(arrayValues);
  Example.steps.push({
    description:"original state",
    parameters:{
      "acc":originalValue,
      "cur": originalValue,
      "idx":"0",
      "src": original
    }
  });
  let acc = 0;
  let cur = originalValue;
  for(var idx = 0; idx<arrayValues.length; idx +=1){
    cur = arrayValues[idx];
    acc = func(acc,cur,idx, original);
    Example.steps.push({
      description:`step ${idx+1}`,
      parameters:{
        "acc": acc,
        "cur":cur,
        "idx":idx,
        "src": original
      }
    });
  }
//(acc,cur,idx,src)=>{return acc+cur;}
  return <>
  <div className="row">
  <div className="col">
  <h2>{Example.name}</h2>
  </div>
</div>
<div className="row">
  <div className="pull-right">
         <Button onClick={()=>setCurrentStep(currentStep-1)} disabled={currentStep < 1}><FaAngleLeft/></Button>
         <Button onClick={()=>setCurrentStep(currentStep+1)}  disabled={currentStep > Example.steps.length -2}><FaAngleRight/></Button>
  </div>
</div>
<div className="row">
  <div className="col">
    <div className="card card-body bg-light">
    {`${JSON.stringify(arrayValues)}.reduce(${func.toString()},${originalValue})`}
    </div>
  </div>
</div>
{arrayValues.length !== 0 ?
<ExampleStep 
{...Example.steps[currentStep]} 
stepNumber={currentStep} 
parameterKeys={Example.parameters} 
/> :
null}
</>

}


export default ()=>
          <ReduceExample arrayValues={[1,2,3,4,5]} func={(acc,cur,idx,src)=>{
            return acc+cur+3;
          }} originalValue={0}
          />
