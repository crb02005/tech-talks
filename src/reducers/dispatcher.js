import React, { useReducer } from 'react';
import Button from 'react-bootstrap/Button';


const add = (operand1, operand2) => operand1 + operand2;
const subtract = (operand1, operand2) => operand1 - operand2;
const calc = (operand1, operand2, operand3) => {
  return operand1 === null ? operand2 : operand1(operand2, operand3);
}
const initialize = (initialValue) => {
  return {
    "initialValue": initialValue,
    "display": "0",
    "operand1": 0,
    "operator": (operand1,operand2)=>operand2
  };
}
const reducer = (state/*acc*/, message/*cur*/) => {

  switch (message.action) {

    case "press":
      return Object.assign({ ...state }, { display: state.display === "0" ? message.key : state.display + message.key });
    case "add":
      return Object.assign({ ...state }, {
        "operand1": parseInt(state.display),
        "operator": add,
        display: "0"
      });
    case "subtract":
      return Object.assign({ ...state }, {
        "operand1": parseInt(state.display),
        "operator": subtract,
        display: "0"
      });
    case "calc":
      let operand2 = parseInt(state.display);
      let calcResult = calc(state.operator, state.operand1, operand2);
      return Object.assign({ ...state }, {
        "display": String(calcResult)
      });
    case "clear":
      return initialize(state.initialValue);
    default:
      throw new Error("unknown operation");
  }
}


export default (props)=>{
    const [state, dispatch] = useReducer(reducer, 0, initialize);
    return <div className="container-fluid">
    <div className="row">
      <div className="col-3">
        Calculator
      </div>
      </div>
      <div className="row">
      <div className="col-3">
        <span className="card card-body bg-light text-right">{state.display}</span>
      </div>
    </div>
    <div className="row text-center">
      <div className="col-1">
      </div>
    </div>
    <div className="row">
    <div className="col-3">
    <Button onClick={() => dispatch({ action: "clear" })}>Clear</Button>
    </div>
    </div>
    <div className="row">
    <div className="col-3">
    <Button onClick={() => dispatch({ action: "press", key: "7" })}>7</Button>
    <Button onClick={() => dispatch({ action: "press", key: "8" })}>8</Button>
    <Button onClick={() => dispatch({ action: "press", key: "9" })}>9</Button>
    </div>
    </div>
    <div className="row">
    <div className="col-3">
    <Button onClick={() => dispatch({ action: "press", key: "4" })}>4</Button>
    <Button onClick={() => dispatch({ action: "press", key: "5" })}>5</Button>
    <Button onClick={() => dispatch({ action: "press", key: "6" })}>6</Button>
    </div>
    </div>
    <div className="row">
    <div className="col-3">
    <Button onClick={() => dispatch({ action: "press", key: "1" })}>1</Button>
    <Button onClick={() => dispatch({ action: "press", key: "2" })}>2</Button>
    <Button onClick={() => dispatch({ action: "press", key: "3" })}>3</Button>
    </div>
    </div>
    <div className="row">
    <div className="col-3">
    <Button onClick={() => dispatch({ action: "add" })}>+</Button>
    <Button onClick={() => dispatch({ action: "press", key: "0" })}>0</Button>
    <Button onClick={() => dispatch({ action: "subtract" })}>-</Button>
    </div>
    </div>
    <div className="row">
    <div className="col-3">
    <Button onClick={() => dispatch({ action: "calc" })}>Calculate</Button>
    </div>
    </div>
  </div>

}