import React from 'react'

export default ()=><>  															Package Managers
<p class="fragment" data-fragment-index="1">1994 - dpkg - Debian Linux - Ian Murdock</p>
<p class="fragment" data-fragment-index="2">1997 - rpm - RedHat Linux - Erik Troan, Marc Ewing</p>
<p class="fragment" data-fragment-index="3">1998 - 
    <a href="https://en.wikipedia.org/wiki/APT_(Debian)" rel="noopener noreferrer" target="_blank">apt</a> - Debian Linux - The Debian Project
</p>
<p class="fragment" data-fragment-index="4">2010 - <a rel="noopener noreferrer" href="https://www.npmjs.com/">npm</a> - Node.js Package Manager - Issac Z. Schlueter, Rebecca Turner, Kat Marchán, others</p>
</>