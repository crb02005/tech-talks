import React, {useState} from 'react';
import About from './about';
import Presentation from './presentation/presentation';
import PackageManagers from './slides/package-managers';
import DiceRollerFunctions from 'trocar-dice-js';
import Dice from 'trocar-react-dice/dice';

export default (props)=>{

const [result,setResult]=useState(-1);

return <Presentation slides={[
  {caption:"Introduction: The Presenter",content:<About/>},
  {caption:"Prerequisites",content:<>
		<h1>Before you start this journey you should know:</h1>
		<ul>
			<li>
				Some JavaScript
			</li>
			<li>
				Source Control
			</li>
			<li>
				Licenses
			</li>
			<li>
				<a href="https://www.json.org/json-en.html">JSON - https://www.json.org/json-en.html</a>
			</li>
			<li>
				<a href="https://docs.gitlab.com/ee/user/markdown.html">Markdown - https://docs.gitlab.com/ee/user/markdown.html</a>
			</li>
		</ul>
  </>},							
  {caption:"Package",content:<>
  <h1>A collection of programs or subroutines with related functionality.</h1>
</>},
{caption:"Package Managers",content:<PackageManagers/>},
  {caption:"Scopes",content:<>
	<div className="bg-primary">

		Assumptions: 
			<ul>
				<li>You are using NPMjs.com</li>
				<li>You have created an account with NPMjs</li>
			</ul>
	</div>
	<h2 className="text-left">Packages can be:</h2>

			<h3 className="text-left">Unscoped - Public</h3>
			<h3 className="text-left">Scoped - Private</h3>
			<h3 className="text-left">Scoped - Public with a command switch</h3>


	<div>

	Example Scope:
	<code>
		@trocar/somepackage
	</code>

	</div>
	
  </>},
  
  {caption:"Package.json Purpose",content:<><h2 className="text-left">
<ul>
	<li>
	"It lists the packages your project depends on"
	</li>
	<li>
	"It specifies versions of a package that your project can use using semantic versioning rules"
	</li>
	<li>
	"It makes your build reproducible, and therefore easier to share with other developers"
	</li>
</ul></h2>
  </>},
  {caption:"package.json (1)",content:<>
  <h2>
From the docs here: <a rel="noopener noreferrer" target="_blank" href="https://docs.npmjs.com/creating-a-package-json-file">https://docs.npmjs.com/creating-a-package-json-file</a>
</h2>
<div className="container">
	<div className="row">
	"It lists the packages your project depends on"	
	</div>
	<div className="row">
		<div className="col">
			<img src="content/buildnpm/dependencies.png" alt="dependencies" />
		</div>
	</div>
</div>

  </>},
  {caption:"package.json (2)",content:<>
	<div className="container">
		<div className="row">
			<div className="col text-center">
			"It specifies versions of a package that your project can use using semantic versioning rules"
			</div>
			<div className="col text-center">
			<img src="content/buildnpm/version.png" alt="version" />
			</div>
		</div>
	<div className="row">
		<div className="col">
		Learn about Semantic Versioning Rules From... The Docs! <a href="https://docs.npmjs.com/about-semantic-versioning">https://docs.npmjs.com/about-semantic-versioning</a>		
		</div>
	</div>
	<div className="row">
		<div className="col text-center">
			My handy flow chart
		</div>
		<div className="col text-center">
			<a href="content/buildnpm/versioning.png" target="_blank"> 
			<img src="content/buildnpm/versioning.png" style={{width:"10em"}} alt="versioning"/>
			</a>

		</div>
	</div>
	<div className="row">
		<div className="col">
		There is a <a href="https://www.youtube.com/embed/kK4Meix58R4">YouTube video from NPM</a> about how it works.
		</div>
	</div>
</div>

  </>},
{caption:"package.json (3)",content:<>
<div className="container">
	<div className="row">
		<div className="col">
			<h2>"It makes your build reproducible, and therefore easier to share with other developers"</h2>
		</div>
	</div>
	<div className="row">
		<div className="col">
			<h1>Recipe for how to build your app.</h1>
		</div>
	</div>
</div>
</>},
{caption:"Description",content:<>
<br/>
	<h2 className="text-justify"> Making an NPM package without a "description" is like <span className='blink'> winking in the dark.</span> You know what you are doing but nobody else does.</h2>
</>},
							
							{caption:"Package.json with init",content:<>
Package.json can be created with 'npm init'. 

You can default values thusly:
<ul>
<li>npm set init.author.email "example-user@example.com"</li>
<li>npm set init.author.name "example_user"</li>
<li>npm set init.license "MIT"</li>

</ul>
							</>},
							
							{caption:"npm login",content:<>
Without signing in you can publish nothing. (unless you want to just have people install from your 
<a href="https://docs.npmjs.com/cli/v6/commands/npm-install" target="_blank" rel="noopener noreferrer"> git repo directly</a>)
							</>},
							{caption:"npm publish",content:<>
	Once you are signed in, you can publish with 'npm publish'
	This will push the repo.
							</>},
							{caption:"import Dice from 'trocar-dice-js'",content:<>
							You may import your package after you publish.
							<br/>
At this point we will look at the code for this page, which uses a package I published:
<br/>
<button onClick={()=>{setResult(DiceRollerFunctions.d20()) }}>Roll D20</button>

Result:{result}
							</>},{
		caption:"Fixing unmaintained packages",content:<>
	Now that you have the skills to publish a package, you can make the world a safer place.

	<h3> Scenario</h3>
	
	<p>
		You have a dependency on a package that is no longer being maintained, or is unresponsive to pull requests, and you need to update its dependencies.
	</p>
	<p>
		Fork the code in github/gitlab/etc.
	</p>
	<p>
		Update the dependencies.
	</p>
	<p>
		Reference your new package.
	</p>
							</>},{caption:"Creating Packaged React Components",content:<>
							Now that you have the skills to publish a package, you share new React Components.
							<p>
							First make your new package:
							<br/>
							<code>
								npx create-react-app &lt;your-component-name&gt;
							</code>
							</p>
							<p>
							Install Babel to turn ES6 into JavaScript for old browsers:
							<br/>
							<code>
								npm install --save-dev @babel/cli @babel/preset-react
							</code>
							</p>
							<p>
							Now update package.json, first to add Babel:
							<code>
								"babel":{'{"presets":["@babel/preset-react"]}'}
							</code>
							</p>
							<p>
								Remember our talk on Semantic Versioning? Update the version.
							</p>
							<p>
								Remember our talk on Scoping? Update the "private" to false.
							</p>
							<p>
								Remember our talk on Description? Update the description to something people will search for.
							</p>
							<p>
							Add a folder for your components. 
							In our case we will name the folder components. Input.
							<br/>
							<code>mkdir src/components</code>
							</p>
							
							
							</>},{caption:"Creating Packaged React Components Continued",content:<>
							<p>
							Add a folder for distribution. 
							In our case we will name the folder dist. Output.
							<br/>
							<code>mkdir dist</code>
							</p>
							<p>
								Add a step in our package.json under the key "scripts" to clean to "dist" folder, run babel to transpile to the dist folder.
								<br/>
								<code>"buildForNpm":"rm -rf dist && mkdir dist && babel src/components --out-dir dist --copy-files && cp package.json dist/ && cp README.md dist/"
								</code>
								<ul>
									<li><a href="https://explainshell.com/explain?cmd=rm+-rf+dist" target="_blank" rel="noopener noreferrer">rm -rf dist</a></li>
									<li><a href="https://explainshell.com/explain?cmd=mkdir+dist" target="_blank" rel="noopener noreferrer">mkdir dist</a></li>
									<li><a href="https://babeljs.io/docs/en/babel-cli#compile-directories" target="_blank" rel="noopener noreferrer">babel src/components --out-dir dist</a><a href="https://babeljs.io/docs/en/babel-cli#copy-files" target="_blank" rel="noopener noreferrer"> --copy-files</a></li>
									<li><a href="https://explainshell.com/explain?cmd=cp+package.json+dist%2F" target="_blank" rel="noopener noreferrer">cp package.json dist/</a></li>
								</ul>
							</p>
							<p>
							Now add a file for our compontent "src/component/widget.js"
							<br/>
							<code>
								import React from 'react';
								export const Widget=()=&gt;&lt;&gt;Some widget&lt;/&gt;
							</code>
							</p>
							
							</>},{caption:"Creating Packaged React Components Continued 3",content:<>
							<p>
							Update your package.json with a "repository" entry.
							<br/>
							"type": "git"<br/>
							"url": "YOUR_REPO_URL_HERE"
							</p>
							<p>Update your readme.md</p>
							<p>
								move the react, react-dom, react-scripts to devDependencies
							</p>
							<p>
								<code>npm run buildForNpm</code>
							</p>
							<p>
								<code>npm login</code>
							</p>
							<p>
								Finally publish:
							</p>
							<p>
								<code>npm publish dist</code>
							</p>

							
							</>},{caption:"Demo",content:<>
							
							<p>
								Now you can use it for another project by npm installing your package.
							</p>
							
							<Dice />

							</>},{caption:"Alternatives to npmjs",content:<>
							
							<ul>
								<li>
									Direct to a git repo
								</li>
								<li>
									Artifactory by JFrog - An "Enterprise Ready"
								</li>
								<li>
									Cloudsmith
								</li>
								<li>
									MyGet
								</li>
							</ul>
							</>}
	
]}>



  {/* // {caption:"",content:<></>}
  // {caption:"",content:<></>} */}
  
</Presentation>}