import React from 'react';
import ExternalLink from './external-link';
export default({children,title})=><header className="App-header">
    <div className="container-fluid">
        <div className="row">
            <div className="col">
                <img src="profile.jpg" alt="Carl Burks" className="float-left" style={{width:"8vh"}}/>
            </div>
            <div className="col">
                <div className="row">
                    <div className="col">
                    <ExternalLink href="https://burksbrand.com">Carl Burks</ExternalLink> - <a href="/">Tech Talks</a>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                    https://gitlab.com/crb02005/tech-talks
                    </div>
                </div>
            </div>
            <div className="col">
                <div className="float-right">
                {children}
                </div>
            </div>
        </div>
    </div>
</header>