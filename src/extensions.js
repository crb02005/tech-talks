import React from 'react';
import Presentation from './presentation/presentation';
import About from './about';
import Picture from './picture';
import SlideTwoCol from './presentation/slide-two-col';
import {FaChrome} from 'react-icons/fa'; 
import { Link } from 'react-router-dom';
export default (props)=><Presentation slides={[
  {caption:"Your Browser, Your Way",content:<SlideTwoCol 
  left={<Picture src="content/extensions/swiss-army-multi-tool.png" fillbackground={true}/>}
  right={<>
      <h2>
    Building a simple browser extension for Chrome Derivatives.
    </h2></>
  }>
  </SlideTwoCol>},
  {caption:"About Me",content:<About/>},
  {caption:<><FaChrome/>Chrome</>,content:<>
  
  <h1>
  You made this?
  <a href="https://knowyourmeme.com/memes/i-made-this">I made this.</a>
  </h1>

  <p>Firefox is still kicking around, 
    but Edge has joined the other browsers who are &nbsp;
    <a href="https://en.wikipedia.org/wiki/Chromium_(web_browser)#Browsers_based_on_Chromium">Chromium</a> Derivatives.
  </p>

  <p>
    There are many good aspects of many browsers and common code, 
    the focus of the talk is building a browser extension for multiple 
    browsers from a common ancestor and that is enabled by this. We won't deeply 
    get into the downside, but in the physical world of plants a lack of genetic 
    diversity contributed greatly to the Corn Leaf Blight and Potato Blight. 
    
    Surely common code between browsers won't decrease user safety./s.  

  </p>

  </>},
  {caption:"Phenominal Cosmic Power, Ity Bity Living Space",content:<>
    <h1>Extensions Packed Vs Unpacked</h1>
    <p>
      Packed = Deployed. Extension is crx. 
    </p>
    <p>
      Unpacked = Development. It is just the folder with the files in it.

    </p>
  </>},
    {caption:"Developer Mode",content:<>
    <p>
      Before you ship an extension, you have to develop it. Before you can develop you need an environment. To get a functional environment for a browser extension you need to enable developer mode.

      </p>
      <h2>On Edge</h2>
      <ul>
        <li>Use the ...</li>
        <li>Open the Extensions</li>
        <li>Use the Developer Mode slider</li>
      </ul>
      <h2>On Chrome</h2>
      <ul>
        <li>Extensions</li>
        <li>Use the Developer Mode slider</li>
      </ul>
  </>},
    {caption:"Publish or Perish",content:<>
    <ul>
      <li>
        <Link>https://chrome.google.com/webstore/category/extensions</Link>
      </li>
      <li>
      <Link>https://microsoftedge.microsoft.com/addons/Microsoft-Edge-Extensions-Home?hl=en-US</Link>
      </li>

    </ul>
    <p>
      Have a browser that isn't listed? Try to find an app store for any of <a href="https://en.wikipedia.org/wiki/Chromium_(web_browser)#Browsers_based_on_Chromium">these</a>.
    </p>
  </>},
      {caption:"Debugger",content:<>
      <ul>
        <li>
          F12 - it works for the page you are on
        </li>
        <li>
          Right Click the icon and "inspect pop-up window" works for the popup
        </li>
        <li>
          ... &gt; Extensions &gt; Inspect views Background page works for the background.
        </li>
      </ul>
    </>},
    {
      caption:"Demos",content:<>
        Demo 1 - Hello world
        <ul>
          <li>Show it logs!</li>
          <li>Show style changes! <a href="https://knowyourmeme.com/memes/hackerman" target="_blank" rel="noopener noreferrer">Green screen</a></li>
        </ul>
        Demo 2 - Background Scripts (Polling is Bad mmkay)
        <ul>
          <li>Show the counter</li>
          <li>Show button click, Alert! (these are usually bad too!)</li>
          <li>Show Listeners</li>
        </ul>
        Demo 2.1 - Background Scripts With a MutationObserver
        <ul>
          <li>MutationObserver</li>
          <li>Other ideas for things you can do!</li>
        </ul>
        Demo 3 - Revenge of the Popup!
        <ul>
          <li>
            Popup Goes the Weasel!
          </li>
        </ul>
      </>
    }
  
 ]}>

  
</Presentation>