import React from 'react';
// import { render } from '@testing-library/react';
import Home from './Home';
import {MemoryRouter as Router} from 'react-router-dom';
import {mount, shallow, configure } from 'enzyme';


import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});
const mountWithRouter = node => mount(<Router>{node}</Router>);
const shallowWithRouter = node => shallow(<Router>{node}</Router>)

test('renders learn react link', () => {
  expect(mountWithRouter(<Home />).text().indexOf("Sqlit1e")).toBeGreaterThan(-1);
});
test('full mount has thing ', () => {
  /* this loads the node and its children */
  expect(mountWithRouter(<Home />).text().indexOf("Red, Green, Refactor")).toBeGreaterThan(-1);
});
test('shallow does not have the thing',()=>{

  /* this loads just the node and placeholders for its children*/
  expect(shallowWithRouter(<Home/>).text().indexOf("Red, Green, Refactor")).toBe(-1);
  
});
