import React from 'react';
import { Link } from 'react-router-dom';
import {FaDocker, FaDatabase, FaDumpsterFire,FaNpm,FaCheckCircle, FaChrome} from 'react-icons/fa'; 
import {SiBabel} from 'react-icons/si';
import {MdFunctions} from 'react-icons/md';

const Card = ({title,subtitle,text,link})=>{
  return <div className="card" style={{width: "18rem"}}>
  <div className="card-body">
    <h5 className="card-title">{title}</h5>
    <h6 className="card-subtitle mb-2 text-muted">{subtitle}</h6>
    <p className="card-text">{text}</p>
    {link}
  </div>
</div>
}

export default (props)=><div className="card-columns">
<Card title={<><FaDatabase/> Sqlite</>} subtitle={"Sqlite, the most popular database on Earth."} text={"Carl survey's of SQLite the database"} link={<Link to="/sqlite" className="card-link"> Adapted Talk from Powerpoint</Link>} />
{
  /*
  TODO: refactor to us common component
  */
}
<div className="card" style={{width: "18rem"}}>
  <div className="card-body">
    <h5 className="card-title"><FaDocker/> Docker</h5>
    <h6 className="card-subtitle mb-2 text-muted">A concise Docker talk</h6>
    <p className="card-text">Containers!</p>
    <Link to="/docker" className="card-link"> Docker containers</Link>
  </div>
</div>
<div className="card" style={{width: "18rem"}}>
  <div className="card-body">
    <h5 className="card-title"><FaDumpsterFire/> Fun With Typescript and Es2018</h5>
    <h6 className="card-subtitle mb-2 text-muted">A Micro Talk on Typescript</h6>
    <p className="card-text">Typescript</p>
    <Link to="/ts" className="card-link"> Typescript</Link>
  </div>
</div>
<div className="card" style={{width: "18rem"}}>
  <div className="card-body">
    <h5 className="card-title"><FaNpm/> NPM</h5>
    <h6 className="card-subtitle mb-2 text-muted">Learning NPM basics</h6>
    <p className="card-text">Node Package Manager</p>
    <Link to="/npm" className="card-link"> NPM </Link>
  </div>
</div>
<div className="card" style={{width: "18rem"}}>
  <div className="card-body">
    <h5 className="card-title"><MdFunctions/> Reducers</h5>
    <h6 className="card-subtitle mb-2 text-muted">Reducing for Fun and Profit</h6>
    <p className="card-text">Learning how reducers work</p>
    <Link to="/reducers" className="card-link"> Reducers </Link>
  </div>
</div>
<div className="card" style={{width: "18rem"}}>
  <div className="card-body">
    <h5 className="card-title"><FaCheckCircle/> Software Testing</h5>
    <h6 className="card-subtitle mb-2 text-muted">Software Testing - </h6>
    <p className="card-text">Testing, TDD, and Javascript</p>
    <Link to="/testing" className="card-link"> Red, Green, Refactor </Link>
  </div>
</div>
<div className="card" style={{width: "18rem"}}>
  <div className="card-body">
    <h5 className="card-title"><FaNpm/> Build your own NPM package</h5>
    <h6 className="card-subtitle mb-2 text-muted">NPM package</h6>
    <p className="card-text">How To Make Your Own NPM Package</p>
    <Link to="/buildnpm" className="card-link"> Your NPM Package </Link>
  </div>
</div>
<div className="card" style={{width: "18rem"}}>
  <div className="card-body">
    <h5 className="card-title"><FaChrome/> Chrome and Derivatives Extensions</h5>
    <h6 className="card-subtitle mb-2 text-muted">For Chrome, Edge and Other Chrome Based Browsers</h6>
    <p className="card-text">Make the Browser Your Own</p>
    <Link to="/extensions" className="card-link"> Your Browser, Your Way</Link>
  </div>
</div>
<Card 
  title="NextJS"
  text="You got your server in my client code."
  subtitle="Framework handling, server rendering, routing and more!"
  link={<Link to="/nextjs" className="card-link">Sometimes you need to do SEO</Link>}
  />
  <Card
  title={<><SiBabel/> Roll Your Own Babel Plugin Adventure</>}
  text="Building a plugin and using it"
  link={<Link to="/babel" className="card-link">Don't worry we'll fix it in post.</Link>}
  />
</div>