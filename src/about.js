import React from 'react';
import Picture from './picture';
import SlideTwoCol from './presentation/slide-two-col';
export default (props)=><SlideTwoCol 
left={<Picture src="./profile.jpg"></Picture>}
right={<>
    A few things I have been:
<ul>
    <li> Project Lead serving multiple agile crews.</li>
    <li> Software Architect</li>
    <li> SQL performance commitee member</li>
    <li> Assistant Vice President</li>
    <li> Technology Engineer Sr for financial applications</li>
</ul>
    A few things I have done:
<ul>
    <li> tinker with microcontrollers</li>
    <li> built an amplifier and hand wound my own guitar pickup.</li>
    <li> woodworking projects</li>
    <li> I like to make things from scratch, but detest not invented here syndrome. Just because you can build it yourself and maybe should for the purpose of an improved understanding, doesn't mean you should use it in production.</li>
    <li>
        <a href="https://www.gitlab.com/crb02005" rel="noopener noreferrer" target="_blank">https://www.gitlab.com/crb02005</a>
    </li>
</ul>

</>}>
</SlideTwoCol>
