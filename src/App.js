import React from 'react';
import { HashRouter as Router, Route } from "react-router-dom";
import Home from './home';
import Sqlite from './sqlite';
import Docker from './docker';
import Npm from './npm';
import BuildNpm from './build-your-own-npm-package';
import Ts from './ts';
import Reducer from './reducers/reducer';
import ReducerDispatch from './reducers/dispatcher';
import ReducerSummary from './reducers/summary';
import ReducerManual from './reducers/manual-reducer';
import Testing from './testing.js'
import Extensions from './extensions'
import NextJS from './nextjs'
import Babel from './babel'
function App() {
  return (
    <div className="App">
      <Router>
        <div>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/babel">
            <Babel/>
          </Route>
          <Route path="/sqlite">
            <Sqlite/>
          </Route>
          <Route path="/docker">
            <Docker/>
          </Route>
          <Route path="/ts">
            <Ts/>
          </Route>
          <Route path="/npm">
            <Npm/>
          </Route>
          <Route path="/buildnpm">
            <BuildNpm/>
          </Route>
          <Route exact path="/reducers">
            <Reducer/>
          </Route>
          <Route path="/reducers/manual-example">
           <ReducerManual/>
          </Route>
          <Route path="/reducers/dispatch-example">
           <ReducerDispatch/>
          </Route>
          <Route path="/reducers/summary-example">
            <ReducerSummary/>
          </Route>
          <Route path="/extensions">
            <Extensions/>
          </Route>
          <Route path="/testing">
            <Testing/>
          </Route>
          <Route path="/nextjs">
            <NextJS/>
          </Route>
        </div>
      </Router>
</div>
  );
}

export default App;
