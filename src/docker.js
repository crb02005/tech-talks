import React from 'react';
import Presentation from './presentation/presentation';
import About from './about';
import Picture from './picture';
export default ()=><Presentation slides={[
  {caption:"Docker",content:<Picture src="content/docker/horizontal-logo-monochromatic-white.png"/>},
  {caption:"About Me",content:<About/>},
  {caption:"Hypervisor",content:<>
<a title="Scsami [CC0]" href="https://commons.wikimedia.org/wiki/File:Hyperviseur.png">
  <Picture fillbackground="true" src="https://upload.wikimedia.org/wikipedia/commons/e/e1/Hyperviseur.png"/></a>
  </>},
  {caption:"Docker vs Traditional Virtualization",content:<a title="User:Maklaan [Public domain]" href="https://commons.wikimedia.org/wiki/File:Docker-linux-interfaces.svg"><img width="512" alt="Docker-linux-interfaces" src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/09/Docker-linux-interfaces.svg/512px-Docker-linux-interfaces.svg.png"/></a>},
{caption:"Why Docker",content:<>
<ul>
<li>Flexible: Even the most complex applications can be containerized. </li>
<li>Lightweight: Containers leverage and share the host kernel. </li>
<li>Interchangeable: You can deploy updates and upgrades on-the-fly. </li>
<li>Portable: You can build locally, deploy to the cloud, and run anywhere. </li>
<li>Scalable: You can increase and automatically distribute container replicas. </li>
<li>Stackable: You can stack services vertically and on-the-fly.</li>
</ul>
*<a href="https://docs.docker.com/get-started/"> https://docs.docker.com/get-started/</a>
</>},
{caption:"How To Get Docker?",content:<>

On Ubuntu Linux: 
<ul>

<li>sudo apt search docker </li>
<li>docker/bionic 1.5-1build1 amd64 - System tray for KDE3/GNOME2 docklet applications </li>
<li>docker-compose/bionic,bionic 1.17.1-2 all  - Punctual, lightweight development environments using Docker </li>
<li>sudo apt install docker docker-compose </li>
<li>sudo groupadd docker </li>
<li>sudo usermod -aG docker $USER </li>

</ul>

restart
</>},
{caption:"Image VS Container",content:<a href="https://stackoverflow.com/questions/23735149/what-is-the-difference-between-a-docker-image-and-a-container"><Picture src="content/docker/stack.png" />
<br/>
https://stackoverflow.com/questions/23735149/what-is-the-difference-between-a-docker-image-and-a-container
</a>},
{caption:"Docker Live Demo",content:<>
<h2>Run from directory to serve</h2>

<h3>
  <code>docker run -dit --name my-apache-app -p 8080:80 -v "$PWD":/usr/local/apache2/htdocs/ httpd
    </code></h3>

</>},

{caption:"Docker Live Demo List Images",content:<>
<h1>
Images:</h1>
<h2><code>docker images</code></h2>
<h1>Just the Ids:</h1>
<h2><code>docker images -q</code> </h2>
</>},

{caption:"Docker Live Demo List Containers",content:<>
<h1>
Running Containers:</h1>
<h2><code>docker ps</code></h2>
<h1>All Containers:</h1>
<h2><code>docker ps -a</code> </h2>
<h1>Just the Ids:</h1>
<h2><code>docker ps -aq</code> </h2>
</>},

{caption:"Docker Live Demo Stop Containers",content:<>
<h1>
Stop a Container:</h1>
<h2><code>docker stop &lt;CONTAINER ID&gt;</code></h2>
<h1>Stop all Containers:</h1>
<h2><code>docker ps -qa |  xargs -I "&#123;&#125;" docker stop &#123;&#125; </code> </h2>
<h1>Remove a Container:</h1>
<h2><code>docker rm &lt;CONTAINER ID&gt;</code> </h2>
<h1>Remove an Image:</h1>
<h2><code>docker rmi  &lt;IMAGE ID&gt;</code> </h2>
<h1>Remove All Images (with force):</h1>
<h2><code>docker images -q | uniq | xargs -I "&#123;&#125;" docker rmi -f &#123;&#125;</code> </h2>
</>},
{caption:"Docker File Live Demo",content:<>
<h2><code>docker build . -t techdemo1 </code></h2>
<h2><code>docker run -dit -p 80:80 techdemo1</code></h2>
</>},
{caption:"Docker File Live Demo",content:<>
<h2><code>docker-compose up </code></h2>
</>},
{caption:"Further Discussion ",content:<>
<h2><a href="https://gitlab.com/crb02005/WebDevSkeleton/blob/master/docker-compose.yaml">https://gitlab.com/crb02005/WebDevSkeleton/blob/master/docker-compose.yaml</a></h2>
</>}
]}>




{/*Remove All Images (with force)  */}



  {/* // {caption:"",content:<></>}
  // {caption:"",content:<></>} */}
  
</Presentation>