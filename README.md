# Tech Talks

This repo houses a collection of Tech Talks which I have presented to tech groups.

Topics Coverage:

* Sqlite
* Docker
* Fun With Typescript and Es2018
* NPM
* Reducers
* Software Testing
* Build your own NPM Package
